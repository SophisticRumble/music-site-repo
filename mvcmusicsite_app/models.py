# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.conf import settings
from django.contrib.auth.models import AbstractUser, User



# Create your models here. from mvcmusicsite_app.models import *

class Basket(models.Model):

    creation_date = models.DateTimeField(auto_now_add=True)
    checked_out = models.BooleanField(default=False)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, null=True)

    def __str__(self):
        return str(self.user)

    @property
    def items(self):
        if self.basketitem_set.exists():
            return self.basketitem_set.count()
        return 0

class Genre(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name

class Artist(models.Model):

    name = models.CharField(max_length=100)
    genres = models.ManyToManyField(Genre, blank=True)

    def __str__(self):
        return self.name

class Album(models.Model):

    title = models.CharField(max_length=100)
    year = models.DateField(null=True)
    price = models.DecimalField(max_digits=5, decimal_places=2)
    artists = models.ManyToManyField(Artist, blank=True)
    genre = models.ForeignKey(Genre, null=True, on_delete=models.SET_NULL)


    def __str__(self):
        return self.title

class BasketItem(models.Model):

    basket = models.ForeignKey(Basket, on_delete=models.CASCADE)
    item = models.ForeignKey(Album, on_delete=models.CASCADE)
    quantity = models.PositiveSmallIntegerField(default=1)

    def __str__(self):
        return self.item.title
