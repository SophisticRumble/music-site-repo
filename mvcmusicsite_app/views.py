# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.urls import reverse, reverse_lazy
from django.views import generic
from django.views.generic import TemplateView, CreateView
from django.views.generic.list import ListView
from mvcmusicsite_app.models import Album, Artist, Genre, BasketItem, Basket
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django_filters.views import FilterView
from .forms import UserCreateForm
from django.contrib.auth import authenticate, login
from .utils import get_basket




import django_filters
import json


# Create your views here.

class CreateAccount(CreateView):

    redirect_field_name = 'music_site:home'

    template_name = 'create_account_form.html'
    form_class = UserCreateForm
    success_url = reverse_lazy('music_site:home')

    def form_valid(self, form):

        context = super(CreateAccount, self).form_valid(form)

        print (self.object.first_name)

        login(self.request, self.object)
        basket = Basket.objects.get(id=self.request.session['basket'])
        basket.user = self.object
        basket.save()

        return context





class BasketItemsListView(ListView):
    """
    View listing items in basket of current user.
    """
    def get_context_data(self, **kwargs):
    	context = super(BasketItemsListView, self).get_context_data(**kwargs)
    	context['page'] = 'my_basket'
        context['basket'] = get_basket(self.request)
    	return context

    template_name ='basket_item_view_current_user.html'
    model = BasketItem
    paginate_by = 15

    def get_queryset(self):

        basket = get_basket(self.request)
        return basket.basketitem_set.order_by('item')


class Home(TemplateView):

    template_name = 'home.html'

    def get_context_data(self, **kwargs):
    	context = super(Home, self).get_context_data(**kwargs)
    	context['page'] = 'home'
        context['basket'] = get_basket(self.request)
    	return context

class Checkout(TemplateView):

    template_name = 'checkout.html'


class GenreList(ListView):

    template_name = 'genres.html'
    model = Genre
    paginate_by = 15

    def get_context_data(self, **kwargs):
        context = super(GenreList, self).get_context_data(**kwargs)
        context['page'] = 'genres'
        context['basket'] = get_basket(self.request)
        return context

class ArtistList(ListView):

    template_name = 'artists.html'
    model = Artist
    paginate_by = 15

    def get_context_data(self, **kwargs):
        context = super(ArtistList, self).get_context_data(**kwargs)
        context['page'] = 'artists'
        context['basket'] = get_basket(self.request)
        return context


def add_to_basket(request):

    data = {}
    data['success'] = False

    item_id = request.POST.get('album_id', 0)

    basket = get_basket(request)

    try:

        basketitem = Album.objects.get(id=item_id)

    except Album.DoesNotExist:
        return JsonResponse(data)

    BasketItem.objects.create(basket=basket, item=basketitem)
    data['success'] = True

    return JsonResponse(data)


    #
    # if request.user.is_authenticated:
    #     basket, _ = Basket.objects.get_or_create(user=request.user)
    #     try:
    #
    #         basketitem = Album.objects.get(id=item_id)
    #
    #     except Album.DoesNotExist:
    #         return JsonResponse(data)
    #
    #     BasketItem.objects.create(basket=basket, item=basketitem)
    #     data['success'] = True
    #
    #     return JsonResponse(data)
    #
    # elif not request.user.is_authenticated:
    #
    #     data = {}
    #     data['success'] = False
    #     item_id = request.POST.get('album_id', 0)
    #
    #
    #     if 'Basket' in request.session:
    #
    #         print (request.session['Basket'])
    #         basket = Basket.objects.get(id=request.session['Basket'])
    #
    #         try:
    #             basketitem = Album.objects.get(id=item_id)
    #
    #         except Album.DoesNotExist:
    #             return JsonResponse(data)
    #
    #         BasketItem.objects.create(basket=basket, item=basketitem)
    #         data['success'] = True
    #
    #         return JsonResponse(data)
    #
    #
    #     else:
    #
    #         basket = Basket.objects.create()
    #         request.session['Basket'] = basket.id
    #
    #         try:
    #             basketitem = Album.objects.get(id=item_id)
    #
    #         except Album.DoesNotExist:
    #             return JsonResponse(data)
    #
    #         BasketItem.objects.create(basket=basket, item=basketitem)
    #         data['success'] = True
    #
    #         return JsonResponse(data)



@login_required
def remove_from_basket(request):

    data = {}
    basketitem_id = request.POST.get('basket_item_id', 0)

    data['success'] = False

    try:
        basket_item = BasketItem.objects.filter(id=basketitem_id)

    except BasketItem.DoesNotExist:
        return JsonResponse(data)

    basket_item.delete()
    data['success'] = True

    return JsonResponse(data)




# class AlbumList(ListView):
#
#     template_name = 'albums.html'
#     model = Album
#     paginate_by = 15
#
#     def get_context_data(self, **kwargs):
#         context = super(AlbumList, self).get_context_data(**kwargs)
#         context['page'] = 'albums'
#         return context


class LogoutView(TemplateView):

    template_name = 'registration/logout.html'



class AlbumFilter(django_filters.FilterSet):
    artists__name = django_filters.CharFilter(lookup_expr='icontains')
    genre__name = django_filters.CharFilter(lookup_expr='icontains')
    title = django_filters.CharFilter(lookup_expr='icontains')

    class Meta:
        model = Album
        exclude = ['year', 'price', 'genre', 'artists', 'title']



def album_list(request):

    def get_context_data(self, **kwargs):
        context = super(AlbumFilter, self).get_context_data(**kwargs)
        context['basket'] = get_basket(self.request)
        return context

    f = AlbumFilter(request.GET, queryset=Album.objects.all().order_by('title'))
    return render(request, 'album_filter.html', {'filter': f})
