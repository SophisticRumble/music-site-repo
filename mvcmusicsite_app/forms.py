from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from django import forms

class UserCreateForm(UserCreationForm):

    class Meta:
        model = User
        fields = ("username", "first_name", "last_name", "email", "password1", "password2")
        help_texts = {
        'username': None
        }


    def save(self, commit=True):
        user = super(UserCreateForm, self).save(commit=False)
        user.email = self.cleaned_data["email"]
        if commit:
            user.save()
        return user
