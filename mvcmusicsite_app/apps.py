# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.apps import AppConfig


class MvcmusicsiteAppConfig(AppConfig):
    name = 'mvcmusicsite_app'
