from .models import Basket


def get_basket(request):

    basket = None

    if request.user.is_authenticated:
        basket, _ = Basket.objects.get_or_create(user=request.user)
        print ('only should be going here')
    else:
        if 'basket' in request.session:
            basket = Basket.objects.get(id=request.session['basket'])

        else:
            basket = Basket.objects.create()
            request.session['basket'] = basket.id

    return basket
