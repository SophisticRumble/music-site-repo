
$(document).ready(function() {
    $('.js-delete-album').click(function() {
        var basket_item_id = $(this).data("basket_item");

        var data = {
            "basket_item_id": basket_item_id
        };

        $.ajax({
            type: "POST",
            url: remove_basket_url,
            data: data
        }).done(function(data) {

            if (data['success']) {
                console.log('hell yes you removed that from your basket');
                $('#item_' + basket_item_id).remove();
                $(function() {
                    Materialize.toast('Removed item from basket', 4000);
                });
            } else {
                console.log('nope - this one is staying in your basket');
                $(function() {
                    Materialize.toast('Failed to remove from basket', 4000);
                });
            }

        });

    });

});
