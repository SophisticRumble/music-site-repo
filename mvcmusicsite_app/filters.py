import django_filters

class AlbumFilter(django_filters.FilterSet):
    artists__name = django_filters.CharFilter(lookup_expr='icontains')

    class Meta:
        model = Album
