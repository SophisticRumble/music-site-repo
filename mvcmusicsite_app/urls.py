from django.conf.urls import url

from . import views
from mvcmusicsite_app.models import Album
from django_filters.views import FilterView

app_name = 'music_site'
urlpatterns = [
    url(r'^$', views.Home.as_view(), name='home'),
    url(r'^home/$', views.Home.as_view(), name='home'),
    url(r'^genres/$', views.GenreList.as_view(), name='genres'),
    url(r'^albums/$', views.album_list, name='albums'),
    url(r'^artists/$', views.ArtistList.as_view(), name='artists'),
    url(r'^album/add_to_basket/$', views.add_to_basket, name='add_to_basket'), # dont use ajax in your url name - needs to make sense on what object you acting on
    url(r'^mybasket/$', views.BasketItemsListView.as_view(), name='my_basket'),
    url(r'^album/remove_from_basket/$', views.remove_from_basket, name='remove_from_basket'),
    url(r'^signup/$', views.CreateAccount.as_view(), name='signup'),
    url(r'^checkout/$', views.Checkout.as_view(), name='checkout'),
]
