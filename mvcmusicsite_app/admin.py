# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from mvcmusicsite_app.models import Album, Artist, Genre, BasketItem, Basket

# Register your models here.

admin.site.register(Album)

admin.site.register(Artist)

admin.site.register(Genre)

admin.site.register(BasketItem)

admin.site.register(Basket)
